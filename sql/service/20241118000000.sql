USE mailserver;

DROP TABLE IF EXISTS `mailboxes`;
DROP TABLE IF EXISTS `mailboxes_domains`; 

CREATE TABLE `aliases` (
	id INT UNSIGNED NOT NULL,
	email varchar(128) NOT NULL,
	CONSTRAINT aliases_pk PRIMARY KEY (id, email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `redirections` (
	id INT UNSIGNED NOT NULL,
	email varchar(128) NOT NULL,
	CONSTRAINT redirections_pk PRIMARY KEY (id, email)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `recipient_bcc` (
	id INT UNSIGNED NOT NULL,
	email varchar(128) NOT NULL,
	CONSTRAINT recipient_bcc_pk PRIMARY KEY (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sender_bcc` (
	id INT UNSIGNED NOT NULL,
	email varchar(128) NOT NULL,
	CONSTRAINT sender_bcc_pk PRIMARY KEY (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `transport` (
	id INT UNSIGNED NOT NULL,
	transport varchar(128) NOT NULL,
	CONSTRAINT transport_pk PRIMARY KEY (id, transport)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `quota` (
	id INT UNSIGNED NOT NULL,
	quota BIGINT(11) NOT NULL DEFAULT 0,
	CONSTRAINT transport_pk PRIMARY KEY (id, quota)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
