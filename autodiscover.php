<?php
header('Content-type: application/xml');
$data = file_get_contents("php://input");
file_put_contents('/srv/test.log', date('Y-m-d H:i:s') . ':' . $data);
preg_match("/\<EMailAddress\>(.*?)\<\/EMailAddress\>/", $data, $matches);

echo '<?xml version="1.0" encoding="utf-8"?>';
?>

<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
	<Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
		<Account>
			<AccountType>email</AccountType>
			<Action>settings</Action>
			<Protocol>
				<Type>IMAP</Type>
				<TTL>1</TTL>
				<Server>imap.<?= getenv('DOMAIN');?></Server>
				<Port>993</Port>
				<LoginName><?= $matches[1]; ?></LoginName>
				<DomainRequired>true</DomainRequired>
				<Domain><?= getenv('DOMAIN');?></Domain>
				<SPA>off</SPA>
				<SSL>on</SSL>
				<AuthRequired>on</AuthRequired>
			</Protocol>
			<Protocol>
				<Type>SMTP</Type>
				<TTL>1</TTL>
				<Server>smtp.<?= getenv('DOMAIN');?></Server>
				<Port>587</Port>
				<LoginName><?= $matches[1]; ?></LoginName>
				<DomainRequired>true</DomainRequired>
				<Domain><?= getenv('DOMAIN');?></Domain>
				<SPA>off</SPA>
				<SSL>on</SSL>
				<AuthRequired>on</AuthRequired>
			</Protocol>
		</Account>
	</Response>
</Autodiscover>