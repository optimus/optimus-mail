<?php
if ($_SERVER["HTTP_AUTH_PROTOCOL"] == "imap")
	$port=143;
else if ($_SERVER["HTTP_AUTH_PROTOCOL"] == "smtp")
	$port=587;

//CONNECT TO DATABASE
$connection = new PDO("mysql:host=optimus-databases", getenv('MARIADB_API_USER'), getenv('MARIADB_API_PASSWORD'));

//ACCEPT CONNECTION IF RECIPIENT DOMAIN IS IN OUR MANAGED DOMAINS
if ($_SERVER["HTTP_AUTH_SMTP_TO"])
{
	$domains_query = $connection->query("SELECT domain FROM server.domains WHERE status = 1");
	while ($domain = $domains_query->fetch(PDO::FETCH_ASSOC))
		if (strpos($_SERVER["HTTP_AUTH_SMTP_TO"], '@' . $domain['domain']))
		{
			file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - " . $_SERVER["HTTP_AUTH_SMTP_FROM"] .  $_SERVER["HTTP_AUTH_SMTP_TO"] . "\n", FILE_APPEND);
			header('Auth-Status: OK');
			header('Auth-Server: ' . getHostByName(getHostName()));
			header('Auth-Port: ' . $port);
			header('Auth-User: ' . $_SERVER['HTTP_AUTH_USER']);
			header('Auth-Pass: ' . $_SERVER['HTTP_AUTH_PASS']);
			http_response_code(200);
			exit;
		}
}

//VALIDATE INPUTS
if (!isset($_SERVER["HTTP_AUTH_USER"] ) || $_SERVER["HTTP_AUTH_USER"] == '')
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - No username submitted\n", FILE_APPEND);
	header("Auth-Status: no username submitted");
	header("Auth-Wait: 3");
	exit;
}

if (!isset($_SERVER["HTTP_AUTH_PASS"] ) || $_SERVER["HTTP_AUTH_PASS"] == '')
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : ".  $_SERVER['HTTP_CLIENT_IP'] . " - No password submitted by " . $_SERVER["HTTP_AUTH_USER"] . "\n", FILE_APPEND);
	header("Auth-Status: no password submitted");
	header("Auth-Wait: 3");
	exit;
}

//PLAIN PASSWORD CHECK
$exists = $connection->prepare("SELECT id, email, password FROM `server`.`users` WHERE email=:email AND status = 1 LIMIT 0,1");
$exists->bindParam(':email', $_SERVER['HTTP_AUTH_USER'], PDO::PARAM_STR);
$exists->execute();
if($exists->rowCount() != 1)
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - Unknown user " . $_SERVER["HTTP_AUTH_USER"] . " (PLAIN)\n", FILE_APPEND);
	header("Auth-Status: Unknown user");
	header("Auth-Wait: 3");
	exit;
}
$user = $exists->fetch(PDO::FETCH_OBJ);
if (password_verify($_SERVER["HTTP_AUTH_PASS"], $user->password))
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - Successful login for " . $_SERVER["HTTP_AUTH_USER"] . " (PLAIN)\n", FILE_APPEND);
	header('Auth-Status: OK');
	header('Auth-Server: ' . getHostByName(getHostName()));
	header('Auth-Port: ' . $port);
	header('Auth-User: ' . $_SERVER['HTTP_AUTH_USER']);
	header('Auth-Pass: ' . $_SERVER['HTTP_AUTH_PASS']);
	http_response_code(200);
	exit;
}

//BCRYPT PASSWORD TEST
$exists = $connection->prepare("SELECT id, email, password FROM `server`.`users` WHERE email=:email AND status = 1 LIMIT 0,1");
$exists->bindParam(':email', $_SERVER['HTTP_AUTH_USER'], PDO::PARAM_STR);
$exists->execute();
if($exists->rowCount() != 1)
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - Unknown user " . $_SERVER["HTTP_AUTH_USER"] . " (BCRYPT)\n", FILE_APPEND);
	header("Auth-Status: Unknown user");
	header("Auth-Wait: 3");
	http_response_code(401);
	exit;
}
$user = $exists->fetch(PDO::FETCH_OBJ);
if ($user->password == $_SERVER['HTTP_AUTH_PASS'])
{
	file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - Successful login for " . $_SERVER["HTTP_AUTH_USER"] . " (BCRYPT)\n", FILE_APPEND);
	header('Auth-Status: OK');
	header('Auth-Server: ' . getHostByName(getHostName()));
	header('Auth-Port: ' . $port);
	header('Auth-User: ' . $_SERVER['HTTP_AUTH_USER']);
	header('Auth-Pass: ' . $_SERVER['HTTP_AUTH_PASS']);
	http_response_code(200);
	exit;
}

file_put_contents("/var/log/optimus/auth-optimus-mail.log", date('Y-m-d H:i:s.u') . " : " .  $_SERVER['HTTP_CLIENT_IP'] . " - Invalid password for " . $_SERVER["HTTP_AUTH_USER"] . "\n", FILE_APPEND);

header("Auth-Status: wrong password");
header("Auth-Wait: 3");
exit;
?>
