<?php
$get = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent lister les destinataires");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$recipient_bcc = $connection->query("SELECT email FROM `mailserver`.`recipient_bcc` WHERE id = '" . $input->owner . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	
	return array("code" => 200, "data" => array_column($recipient_bcc,'email'));
};


$post = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', urldecode($input->path[3]), 'email', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent ajouter un destinataire");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$is_the_same = $connection->query("SELECT email FROM `server`.`users` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
	if ($input->email == $is_the_same->email)
		return array("code" => 400, "message" => "Vous ne pouvez pas ajouter l'adresse elle même comme destinataire");

	$unique = $connection->query("SELECT email FROM `mailserver`.`recipient_bcc` WHERE id = '" . $input->owner . "'");
	if ($unique->rowCount() > 0)
		return array("code" => 400, "message" => "Vous ne pouvez configurer qu'un seul destinataire par boîte mail");

	$insert = $connection->prepare("INSERT INTO `mailserver`.`recipient_bcc` SET id = :id, email = :email");
	$insert->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$insert->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($insert->execute())
		return array("code" => 201);
	else
		return array("code" => 400, "message" => $insert->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', urldecode($input->path[3]), 'email', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent supprimer un destinataire");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$already_used = $connection->prepare("SELECT email FROM `mailserver`.`recipient_bcc` WHERE id = :id AND email = :email");
	$already_used->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$already_used->bindParam(':email', $input->email, PDO::PARAM_STR);
	$already_used->execute(); 
	if ($already_used->rowCount() == 0)
		return array("code" => 404, "message" => "Ce destinataire n'existe pas");

	$delete = $connection->prepare("DELETE FROM `mailserver`.`recipient_bcc` WHERE id = :id AND email = :email");
	$delete->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$delete->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($delete->execute())
		return array("code" => 200);
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
};
?>
