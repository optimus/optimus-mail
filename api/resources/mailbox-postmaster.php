<?php
$get = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent lister les alias");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$aliases = $connection->query("SELECT email FROM `mailserver`.`aliases` WHERE id = '" . $input->owner . "' AND email = 'postmaster@" . getenv('DOMAIN') . "' ORDER BY email")->fetch(PDO::FETCH_ASSOC);
	
	return array("code" => 200, "data" => $aliases['email'] ? true : false);
};


$post = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();
	admin_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', 'postmaster@' . getenv('DOMAIN'), 'email', true);

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$already_exists = $connection->prepare("SELECT email FROM `mailserver`.`aliases` WHERE id = :id AND email = :email");
	$already_exists->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$already_exists->bindParam(':email', $input->email, PDO::PARAM_STR);
	$already_exists->execute(); 
	if ($already_exists->rowCount() > 0)
		return array("code" => 201);

	$insert = $connection->prepare("INSERT INTO `mailserver`.`aliases` SET id = :id, email = :email");
	$insert->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$insert->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($insert->execute())
		return array("code" => 201);
	else
		return array("code" => 400, "message" => $insert->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();
	admin_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', 'postmaster@' . getenv('DOMAIN'), 'email', true);

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$postmasters = $connection->prepare("SELECT email FROM `mailserver`.`aliases` WHERE email = :email");
	$postmasters->bindParam(':email', $input->email, PDO::PARAM_STR);
	$postmasters->execute(); 
	if ($postmasters->rowCount() == 1)
		return array("code" => 400, "message" => "Cet utilisateur est le dernier postmaster. Il faut conserver au minimum un postmaster.");

	$delete = $connection->prepare("DELETE FROM `mailserver`.`aliases` WHERE id = :id AND email = :email");
	$delete->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$delete->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($delete->execute())
		return array("code" => 200);
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
};
?>
