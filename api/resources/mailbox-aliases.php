<?php
$get = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent lister les alias");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$aliases = $connection->query("SELECT email FROM `mailserver`.`aliases` WHERE id = '" . $input->owner . "' AND email != 'postmaster@" . getenv('DOMAIN') . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	
	return array("code" => 200, "data" => array_column($aliases,'email'));
};


$post = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', urldecode($input->path[3]), 'email', true);
	$input->domain = check('domain', explode('@', urldecode($input->path[3]))[1], 'domain', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent ajouter un alias");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	if (!exists($connection, 'server', 'domains', 'domain', $input->domain))
		return array("code" => 400, "message" => "Le serveur mail n'est pas configuré pour recevoir des courriels émanant du domaine " . $input->domain);

	if (exists($connection, 'server', 'users', 'email', $input->email))
		return array("code" => 409, "message" => "Un autre utilisateur utilise déjà cette adresse");

	if ($input->email == 'postmaster@' . getenv('DOMAIN'))
		return array("code" => 400, "message" => "Les postmasters doivent être configuré avec le point d'accès mailbox-postmaster");

	$already_used = $connection->prepare("SELECT email FROM `mailserver`.`aliases` WHERE id != :id AND email = :email");
	$already_used->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$already_used->bindParam(':email', $input->email, PDO::PARAM_STR);
	$already_used->execute(); 
	if ($already_used->rowCount() > 0)
		return array("code" => 409, "message" => "Un autre utilisateur utilise déjà cet alias");

	$already_exists = $connection->prepare("SELECT email FROM `mailserver`.`aliases` WHERE id = :id AND email = :email");
	$already_exists->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$already_exists->bindParam(':email', $input->email, PDO::PARAM_STR);
	$already_exists->execute(); 
	if ($already_exists->rowCount() > 0)
		return array("code" => 409, "message" => "Cet alias existe déjà");

	$insert = $connection->prepare("INSERT INTO `mailserver`.`aliases` SET id = :id, email = :email");
	$insert->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$insert->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($insert->execute())
		return array("code" => 201);
	else
		return array("code" => 400, "message" => $insert->errorInfo()[2]);
};

$delete = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->email = check('email', urldecode($input->path[3]), 'email', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent supprimer un alias");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$already_exists = $connection->prepare("SELECT email FROM `mailserver`.`aliases` WHERE id = :id AND email = :email");
	$already_exists->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$already_exists->bindParam(':email', $input->email, PDO::PARAM_STR);
	$already_exists->execute(); 
	if ($already_exists->rowCount() == 0)
		return array("code" => 400, "message" => "Cet alias n'existe pas");

	if ($input->email == 'postmaster@' . getenv('DOMAIN'))
		return array("code" => 400, "message" => "Les postmasters doivent être configuré avec le point d'accès mailbox-postmaster");

	$delete = $connection->prepare("DELETE FROM `mailserver`.`aliases` WHERE id = :id AND email = :email");
	$delete->bindParam(':id', $input->owner, PDO::PARAM_INT);
	$delete->bindParam(':email', $input->email, PDO::PARAM_STR);
	if ($delete->execute())
		return array("code" => 200);
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
};
?>
