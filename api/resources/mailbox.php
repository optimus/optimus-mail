<?php
$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul le propriétaire de la boite mail ou un administrateur peuvent lister les alias");

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	$user = $connection->query("SELECT email FROM `server`.`users` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_ASSOC);
	$aliases = $connection->query("SELECT email FROM `mailserver`.`aliases` WHERE id = '" . $input->owner . "' AND email != 'postmaster@" . getenv('DOMAIN') . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	$redirections = $connection->query("SELECT email FROM `mailserver`.`redirections` WHERE id = '" . $input->owner . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	$recipient_bcc = $connection->query("SELECT email FROM `mailserver`.`recipient_bcc` WHERE id = '" . $input->owner . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	$sender_bcc = $connection->query("SELECT email FROM `mailserver`.`sender_bcc` WHERE id = '" . $input->owner . "' ORDER BY email")->fetchAll(PDO::FETCH_ASSOC);
	$transport = $connection->query("SELECT transport FROM `mailserver`.`transport` WHERE id = '" . $input->owner . "' ORDER BY transport")->fetchAll(PDO::FETCH_ASSOC);
	$quota = $connection->query("SELECT quota FROM `mailserver`.`quota` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_ASSOC);
	$postmaster = $connection->query("SELECT email FROM `mailserver`.`aliases` WHERE id = '" . $input->owner . "' AND email = 'postmaster@" . getenv('DOMAIN') . "'")->fetch(PDO::FETCH_ASSOC);

	$mailbox = array(
		"email" => $user['email'],
		"aliases" => array_column($aliases,'email'),
		"redirections" => array_column($redirections,'email'),
		"recipient_bcc" => array_column($recipient_bcc,'email'),
		"sender_bcc" => array_column($sender_bcc,'email'),
		"transport" => array_column($transport,'email'),
		"quota" => $quota['quota'],
		"postmaster" => $postmaster['email'] ? true : false
	);
	
	return array("code" => 200, "data" => $mailbox);
};
?>
