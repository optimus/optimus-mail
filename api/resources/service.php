<?php
$get = function ()
{
	global $connection;
	auth();
	allowed_origins_only();
	$service_name = 'optimus-mail';

	$query = $connection->prepare("SELECT status, manifest FROM `server`.`services` WHERE name = :name");
	$query->bindParam(":name", $service_name);
	$query->execute();
	$service = $query->fetch(PDO::FETCH_OBJ);
	$output = json_decode($service->manifest, false);
	$output->status = $service->status;

	return array("code" => 200, "data" => $output);
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-mail';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut activer ce service");
	
	$is_installed = $connection->prepare("SELECT * FROM server.users_services WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'");
	$is_installed->execute();
	if ($is_installed->rowCount() != 0)
		return array("code" => 400, "message" => "Le service " . $service_name . " est déjà activé pour cet utilisateur");

	if (getenv('SMTP') == 'ovh')
	{
		include('/srv/api/libs/ovh.php');
		$mail_password = substr(base64_encode(openssl_encrypt('ovh_smtp_' . getenv('DOMAIN'), 'aes-128-ecb', getenv('AES_KEY'))), 0, 24);
		$mailbox = ovh_api_request('POST', '/email/domain/' . getenv('DOMAIN') . '/account', '{"accountName": "' . explode('@',get_user_email($input->owner))[0] . '", "password": "' . $mail_password . '", "size": 2500000}');
		if (substr($mailbox->message,0,33) == "Maximum pop account quota reached")
			return array("code" => 400, "message" => "Vous ne pouvez pas créer de boîte mail supplémentaire. Vous devez faire évoluer votre offre OVH MX PLAN.");
	}
	
	if (!$connection->query("REPLACE INTO `server`.`users_services` SET user = '" . $input->owner . "', service = '" . $service_name . "'"))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	if (is_admin($input->user->id))
	{
		$postmaster_exists = $connection->query("SELECT email FROM `mailserver`.`aliases` WHERE email LIKE '%postmaster@" . getenv('DOMAIN') . "%'")->fetch(PDO::FETCH_OBJ);
		if (!$postmaster_exists)
			if (!$connection->query("INSERT INTO `mailserver`.`aliases` SET id = '" . $input->owner . "' , email = 'postmaster@" . getenv('DOMAIN') . "'"))
				$errors[] = $connection->errorInfo()[2];
	}

	umask(0);
	if (!file_exists('/srv/mailboxes/') . get_user_email($input->owner))
		@mkdir('/srv/mailboxes/' . get_user_email($input->owner));
	@chmod('/srv/mailboxes/' . get_user_email($input->owner), 0770);
	@chown('/srv/mailboxes/' . get_user_email($input->owner), 'www-data');
	@chgrp('/srv/mailboxes/' . get_user_email($input->owner), 'mailboxes');

	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 201);
};


$patch = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-mail';

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);

	$input->body->email = check('email', $input->body->email, 'email', true);
	
	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur ou l'utilisateur lui même peuvent modifier un utilisateur");

	if (!exists($connection, 'server','users', 'id', $input->owner))
		return array("code" => 404, "message" => "Erreur - cet utilisateur n'existe pas");

	$is_installed = $connection->prepare("SELECT * FROM server.users_services WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'");
	$is_installed->execute();
	if ($is_installed->rowCount() == 0)
		return array("code" => 400, "message" => "Le service " . $service_name . " n'est pas activé pour cet utilisateur");
	
	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");

	if (exists($connection, 'mailserver','aliases', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur utilise déjà cette adresse comme alias");

	if (getenv('SMTP') == 'ovh')
	{
		include('/srv/api/libs/ovh.php');
		$mailbox = ovh_api_request('DELETE', '/email/domain/' . getenv('DOMAIN') . '/account/' . explode('@',get_user_email($input->owner))[0]);
		$mail_password = substr(base64_encode(openssl_encrypt('ovh_smtp_' . getenv('DOMAIN'), 'aes-128-ecb', getenv('AES_KEY'))), 0, 24);
		$mailbox = ovh_api_request('POST', '/email/domain/' . getenv('DOMAIN') . '/account', '{"accountName": "' . explode('@',$input->body->email)[0] . '", "password": "' . $mail_password . '", "size": 2500000}');
	}
	
	if (isset($input->body->email) && $input->body->email != get_user_email($input->owner))
		if (is_dir('/srv/mailboxes/' . get_user_email($input->owner)))
			rename('/srv/mailboxes/' . get_user_email($input->owner), '/srv/mailboxes/' . $input->body->email);
	
	if (isset($input->body->email) && $input->body->email != get_user_email($input->owner))
		if (is_dir('/srv/mailboxes/gpg-keys/' . get_user_email($input->owner)))
			rename('/srv/mailboxes/gpg-keys/' . get_user_email($input->owner), '/srv/mailboxes/gpg-keys/' . $input->body->email);
	
	return array("code" => 200);
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$service_name = 'optimus-mail';

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul l'utilisateur lui même ou un administrateur peut désactiver un service");

	if ($_GET['remove_data'] == true)
	{
		$connection->query("DELETE FROM `mailserver`.`aliases` WHERE id = '" . $input->owner . "'");
		$connection->query("DELETE FROM `mailserver`.`redirections` WHERE id = '" . $input->owner . "'");
		$connection->query("DELETE FROM `mailserver`.`recipient_bcc` WHERE id = '" . $input->owner . "'");
		$connection->query("DELETE FROM `mailserver`.`sender_bcc` WHERE id = '" . $input->owner . "'");
		$connection->query("DELETE FROM `mailserver`.`quota` WHERE id = '" . $input->owner . "'");
		$connection->query("DELETE FROM `mailserver`.`transport` WHERE id = '" . $input->owner . "'");
	}

	if (getenv('SMTP') == 'ovh')
	{
		include('/srv/api/libs/ovh.php');
		$mailbox = ovh_api_request('DELETE', '/email/domain/' . getenv('DOMAIN') . '/account/' . explode('@',get_user_email($input->owner))[0]);
	}
		
	if (!$connection->query("DELETE FROM `server`.`users_services` WHERE user = '" . $input->owner . "' AND service = '" . $service_name . "'"))
		$errors[] = $connection->errorInfo()[2];

	if ($errors)
		return array("code" => 400, "message" => $errors);
	else
		return array("code" => 200);
};
?>