<?php
$patch = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();
	admin_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->size = check('size', $input->path[3], 'positive_integer', true);

	if (!exists($connection, 'server', 'users', 'id', $input->owner))
		return array("code" => 404, "message" => "L'utilisateur n° " . $input->owner . " n'existe pas");

	if ($input->size > 0)
		$insert = $connection->prepare("REPLACE INTO `mailserver`.`quota` SET id = :id, quota = :quota");
	else
		$insert = $connection->prepare("DELETE FROM `mailserver`.`quota` WHERE id = :id");
	$insert->bindParam(':id', $input->owner, PDO::PARAM_INT);
	if ($input->size > 0)
		$insert->bindParam(':quota', $input->size, PDO::PARAM_INT);
	if ($insert->execute())
		return array("code" => 201);
	else
		return array("code" => 400, "message" => $insert->errorInfo()[2]);
};
?>
