<?php
header('Content-type: application/xml');
echo '<?xml version="1.0" encoding="utf-8" ?>';
?>
<clientConfig version="1.1">
	<emailProvider id="<?= getenv('DOMAIN');?>">
		<domain><?= getenv('DOMAIN');?></domain>
		<displayName>Service <?= getenv('DOMAIN');?></displayName>
		<displayShortName><?= getenv('DOMAIN');?></displayShortName>
		<incomingServer type="imap">
			<hostname>imap.<?= getenv('DOMAIN');?></hostname>
			<port>993</port>
			<socketType>SSL</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</incomingServer>
		<outgoingServer type="smtp">
			<hostname>smtp.<?= getenv('DOMAIN');?></hostname>
			<port>587</port>
			<socketType>SSL</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</outgoingServer>
	</emailProvider>
</clientConfig>