export default class contactGeneral
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		return this
	}

	async init()
	{
		await load('/services/optimus-mail/administration/smtp.html', this.target, null, true)
		this.target.querySelectorAll('.smtp_domain').forEach(span => span.innerText = store.user.server.replace('api.', 'imap.'))
		this.target.querySelectorAll('.user_email').forEach(span => span.innerText = store.user.email)
		this.target.querySelectorAll('.mail-tester').forEach(button => button.onclick = () => window.open('https://www.mail-tester.com/?lang=fr', '_blank'))

		loader(this.target.querySelector('.smtp_ovh_block'))
		rest(store.user.server + '/optimus-base/ovh/' + store.user.server.replace('api.', '') + '/email', 'GET')
			.then(response =>
			{
				let email = response.data
				if (email?.offer)
				{
					if (email.domain.age < 30 * 24 * 60 * 60)
						this.target.querySelector('.smtp_optimus_young_domain_alert').classList.remove('is-hidden')
					this.target.querySelector('.smtp_ovh_block_details').classList.remove('is-hidden')
					this.target.querySelector('.smtp_ovh_alert').classList.add('is-hidden')
					document.getElementById('ovh_smtp_password').innerText = email?.smtp_password
					document.getElementById('ovh_email_offer').innerText = email?.offer
					document.getElementById('ovh_email_quota').innerText = email?.summary?.account + ' / ' + email?.quota?.account
					document.getElementById('ovh_email_dkim').innerText = email?.dkim?.status == 'enabled' ? 'activé' : 'désactivé'
					document.getElementById('ovh_email_spf').innerText = email?.isSPFValid == 'valid' ? 'valide' : 'invalide'
					document.getElementById('blacklist_check_button').onclick = () => window.open('https://mxtoolbox.com/SuperTool.aspx?action=blacklist:' + email?.server_ip, '_blank')
					document.getElementById('uceprotect_check_button').onclick = () => window.open('https://www.uceprotect.net/en/rblcheck.php?ipr=' + email?.server_ip, '_blank')
					document.getElementById('blacklist_ovh_check_button').onclick = () => window.open('https://mxtoolbox.com/SuperTool.aspx?action=blacklist:ssl0.ovh.net', '_blank')
					document.getElementById('uceprotect_ovh_check_button').onclick = () => window.open('https://www.uceprotect.net/en/rblcheck.php?asn=16276', '_blank')
				}
				loader(this.target.querySelector('.smtp_ovh_block'), false)
			})
	}
}