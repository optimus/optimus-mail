export default class contactGeneral
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		return this
	}

	async init()
	{
		await load('/services/optimus-mail/administration/imap.html', this.target, null, true)
		this.target.querySelectorAll('.imap_domain').forEach(span => span.innerText = store.user.server.replace('api.', 'imap.'))
		this.target.querySelectorAll('.user_email').forEach(span => span.innerText = store.user.email)
	}
}