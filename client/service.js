export default class OptimusMailService
{
	constructor()
	{
		this.name = 'optimus-mail'
		store.services.push(this)
		this.swagger_modules =
			[
				{
					id: "optimus-mail",
					title: "OPTIMUS MAIL",
					path: "/services/optimus-mail/optimus-mail.yaml",
					filters: ["mailbox", "service"]
				}
			]

		if (!admin_only())
			this.optimus_base_administration_tabs =
				[
					{
						id: "tab_imap",
						text: "IMAP",
						link: "/services/optimus-mail/administration/imap.js",
						position: 650
					},
					{
						id: "tab_smtp",
						text: "SMTP",
						link: "/services/optimus-mail/administration/smtp.js",
						position: 651
					}
				]
	}

	login()
	{
		this.optimus_base_user_tabs =
			[
				{
					id: "tab_mailbox",
					text: "Boîte mail",
					link: "/services/optimus-mail/users/mailbox.js",
					position: 150
				}
			]

		topmenus['usermenu'].add('my-mailbox', 'item', 'Ma boîte mail', null, () => router('optimus-mail/my-mailbox'), null, 250)
	}

	logout()
	{
		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{

	}

	dashboard()
	{

	}
}