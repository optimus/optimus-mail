export default class userAccount
{
	constructor() 
	{
	}

	async init()
	{
		store.queryParams.id = store.user.id
		await load('/services/optimus-mail/users/mailbox.js', main)
		main.querySelector('.standalone-title').classList.remove('is-hidden')
	}
}