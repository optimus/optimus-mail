export default class contactGeneral
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.id || store.user.id
		return this
	}

	async init()
	{
		loader(this.target, true)
		await load('/services/optimus-mail/users/mailbox.html', this.target, null, true)

		let mailbox = await rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox').then(response => response.data)
		mailbox.aliases.forEach(alias => this.add_alias(alias))
		mailbox.redirections.forEach(redirection => this.add_redirections(redirection))
		mailbox.recipient_bcc.forEach(recipient => this.add_recipient_bcc(recipient))
		mailbox.sender_bcc.forEach(sender => this.add_sender_bcc(sender))
		this.target.querySelector('.postmaster-switch').checked = mailbox.postmaster

		this.target.querySelectorAll('.user-email').forEach(span => span.innerText = mailbox.email)

		this.target.querySelector('.mailbox-alias-add-button').onclick = () => modal.open('/services/optimus-mail/users/mailbox_alias_add_modal.js', false, this)
		this.target.querySelector('.mailbox-redirections-add-button').onclick = () => modal.open('/services/optimus-mail/users/mailbox_redirections_add_modal.js', false, this)
		this.target.querySelector('.mailbox-recipient-bcc-add-button').onclick = () => modal.open('/services/optimus-mail/users/mailbox_recipient_bcc_add_modal.js', false, this)
		this.target.querySelector('.mailbox-sender-bcc-add-button').onclick = () => modal.open('/services/optimus-mail/users/mailbox_sender_bcc_add_modal.js', false, this)

		this.target.querySelector('.quota-slider').oninput = () => 
		{
			if (this.target.querySelector('.quota-slider').value == 101)
				this.target.querySelector('.quota-slider').dataset.tooltip = 'Illimité'
			else
				this.target.querySelector('.quota-slider').dataset.tooltip = this.target.querySelector('.quota-slider').value + ' Go'
		}
		if (store.user.admin == 0)
		{
			this.target.querySelector('.postmaster-switch').setAttribute('disabled', 'disabled')
			this.target.querySelector('.postmaster-switch').title = "fonction réservée aux administrateurs"
			this.target.querySelector('.quota-slider').setAttribute('disabled', 'disabled')
			this.target.querySelector('.quota-slider').title = "fonction réservée aux administrateurs"
		}
		else
		{
			this.target.querySelector('.postmaster-switch').onchange = () =>
				rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-postmaster', this.target.querySelector('.postmaster-switch').checked ? 'POST' : 'DELETE')
					.then(response => 
					{
						if (response.code > 201)
							this.target.querySelector('.postmaster-switch').checked = !this.target.querySelector('.postmaster-switch').checked
					})
			this.target.querySelector('.quota-slider').onchange = () =>
				rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-quota/' + (this.target.querySelector('.quota-slider').value == 101 ? 0 : this.target.querySelector('.quota-slider').value * 1024 * 1024 * 1024), 'PATCH')
		}

		this.target.querySelector('.quota-slider').value = mailbox.quota ? Math.round(mailbox.quota / 1024 / 1024 / 1024) : 101
		this.target.querySelector('.quota-slider').oninput()

		loader(this.target, false)
	}


	add_alias = email =>
	{
		let template = this.target.querySelector('template#mailbox-alias').content.cloneNode(true)
		template.firstElementChild.id = 'mailbox_alias_' + email
		template.querySelector('.mailbox-alias-input').value = email
		template.querySelector('.mailbox-alias-delete-button').onclick = () => this.remove_alias(email)
		this.target.querySelector('.mailbox-alias-container').appendChild(template)
	}


	remove_alias = email =>
		rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-aliases/' + email, 'DELETE')
			.then(response => response.code == 200 && document.getElementById('mailbox_alias_' + email).remove())


	add_redirections = email =>
	{
		let template = this.target.querySelector('template#mailbox-redirections').content.cloneNode(true)
		template.firstElementChild.id = 'mailbox_redirections_' + email
		template.querySelector('.mailbox-redirections-input').value = email
		template.querySelector('.mailbox-redirections-delete-button').onclick = () => this.remove_redirections(email)
		this.target.querySelector('.mailbox-redirections-container').appendChild(template)
	}

	remove_redirections = email =>
		rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-redirections/' + email, 'DELETE')
			.then(response => response.code == 200 && document.getElementById('mailbox_redirections_' + email).remove())


	add_recipient_bcc = email =>
	{
		let template = this.target.querySelector('template#mailbox-recipient-bcc').content.cloneNode(true)
		template.firstElementChild.id = 'mailbox_recipient_bcc_' + email
		template.querySelector('.mailbox-recipient-bcc-input').value = email
		template.querySelector('.mailbox-recipient-bcc-delete-button').onclick = () => this.remove_recipient_bcc(email)
		this.target.querySelector('.mailbox-recipient-bcc-container').appendChild(template)
		this.target.querySelector('.mailbox-recipient-bcc-add-button').classList.add('is-hidden')
	}

	remove_recipient_bcc = email =>
		rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-recipient-bcc/' + email, 'DELETE')
			.then(response => 
			{
				if (response.code == 200)
				{
					document.getElementById('mailbox_recipient_bcc_' + email).remove()
					this.target.querySelector('.mailbox-recipient-bcc-add-button').classList.remove('is-hidden')
				}
			})


	add_sender_bcc = email =>
	{
		let template = this.target.querySelector('template#mailbox-sender-bcc').content.cloneNode(true)
		template.firstElementChild.id = 'mailbox_sender_bcc_' + email
		template.querySelector('.mailbox-sender-bcc-input').value = email
		template.querySelector('.mailbox-sender-bcc-delete-button').onclick = () => this.remove_sender_bcc(email)
		this.target.querySelector('.mailbox-sender-bcc-container').appendChild(template)
		this.target.querySelector('.mailbox-sender-bcc-add-button').classList.add('is-hidden')
	}

	remove_sender_bcc = email =>
		rest('https://' + this.server + '/optimus-mail/' + this.owner + '/mailbox-sender-bcc/' + email, 'DELETE')
			.then(response => 
			{
				if (response.code == 200)
				{
					document.getElementById('mailbox_sender_bcc_' + email).remove()
					this.target.querySelector('.mailbox-sender-bcc-add-button').classList.remove('is-hidden')
				}
			})
}