export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-mail/users/mailbox_redirections_add_modal.html', this.target)

		modal.querySelector('.mailbox-redirections-input').onkeyup = event => event.key == "Enter" && modal.querySelector('.add-button').click()

		setTimeout(() => modal.querySelector('.mailbox-redirections-input').focus(), 10)

		modal.querySelector('.add-button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-mail/' + this.component.owner + '/mailbox-redirections/' + modal.querySelector('.mailbox-redirections-input').value.toLowerCase(), 'POST', {}, 'toast')
				.then(response => 
				{
					if (response.code == 201) 
					{
						this.component.add_redirections(modal.querySelector('.mailbox-redirections-input').value.toLowerCase())
						modal.close()
					}
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}