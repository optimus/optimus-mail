export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-mail/users/mailbox_alias_add_modal.html', this.target)

		let domains = await rest('https://' + this.component.server + '/optimus-base/domains').then(response => response.data)
		domains.forEach(domain => modal.querySelector('.mailbox-alias-domains').options.add(new Option(domain.domain)))

		modal.querySelector('.mailbox-alias-input').onkeyup = event => event.key == "Enter" && modal.querySelector('.add-button').click()

		setTimeout(() => modal.querySelector('.mailbox-alias-input').focus(), 10)

		modal.querySelector('.add-button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-mail/' + this.component.owner + '/mailbox-aliases/' + modal.querySelector('.mailbox-alias-input').value.toLowerCase() + '@' + modal.querySelector('.mailbox-alias-domains').value.toLowerCase(), 'POST', {}, 'toast')
				.then(response => 
				{
					if (response.code == 201) 
					{
						this.component.add_alias(modal.querySelector('.mailbox-alias-input').value.toLowerCase() + '@' + modal.querySelector('.mailbox-alias-domains').value.toLowerCase())
						modal.close()
					}
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}