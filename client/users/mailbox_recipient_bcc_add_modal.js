export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-mail/users/mailbox_recipient_bcc_add_modal.html', this.target)

		modal.querySelector('.mailbox-recipient-bcc-input').onkeyup = event => event.key == "Enter" && modal.querySelector('.add-button').click()

		setTimeout(() => modal.querySelector('.mailbox-recipient-bcc-input').focus(), 10)

		modal.querySelector('.add-button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-mail/' + this.component.owner + '/mailbox-recipient-bcc/' + modal.querySelector('.mailbox-recipient-bcc-input').value.toLowerCase(), 'POST', {}, 'toast')
				.then(response => 
				{
					if (response.code == 201) 
					{
						this.component.add_recipient_bcc(modal.querySelector('.mailbox-recipient-bcc-input').value.toLowerCase())
						modal.close()
					}
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}