openapi: 3.0.1
info:
  title: OPTIMUS-MAIL
  contact:
    name: Lionel VEST
    email: info@optimus-avocats.fr
    url: "https://www.optimus-avocats.fr"
  license:
    name: GNU Affero General Public License V3.0
    url: "https://git.cybertron.fr/optimus/api-cloud/-/raw/master/LICENSE"
  version: "3.00"
servers:
  - url: "https://{apiserver}/optimus-mail"

paths:
  #RESOURCES
  "/resources/{resource}":
    get:
      summary: Retourne la définition des ressources gérées par le service
      tags:
        - resources
      operationId: resources_get
      parameters:
        - name: resource
          in: path
          description: Nom de la ressource
          required: true
          schema:
            type: string
            enum: ["mailboxes"]
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

 # MAILBOX
  "/{owner}/mailbox":
    get:
      summary: Retourne les informations complètes sur la boite mail de l'utilisateur
      tags:
        - mailboxes
      operationId: mailbox_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

# MAILBOX ALIASES
  "/{owner}/mailbox-aliases":
    get:
      summary: Retourne les alias de la boite mail de l'utilisateur désigné
      tags:
        - mailbox_aliases
      operationId: mailbox_aliases_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

  "/{owner}/mailbox-aliases/{email}":
    post:
      summary: Ajoute un alias à la boite mail de l'utilisateur désigné
      tags:
        - mailbox_aliases
      operationId: mailbox_aliases_add
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Alias à ajouter
          required: true
          schema:
            type: string
            example: "alias@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    delete:
      summary: Supprime un alias de la boite mail de l'utilisateur désigné
      tags:
        - mailbox_aliases
      operationId: mailbox_aliases_delete
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Alias à supprimer
          required: true
          schema:
            type: string
            example: "alias@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


# MAILBOX REDIRECTIONS
  "/{owner}/mailbox-redirections":
    get:
      summary: Retourne la liste des redirections en place sur la boite mail de l'utilisateur désigné
      tags:
        - mailbox_redirections
      operationId: mailbox_redirections_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

  "/{owner}/mailbox-redirections/{email}":
    post:
      summary: Ajoute une redirections à la boite mail de l'utilisateur désigné
      tags:
        - mailbox_redirections
      operationId: mailbox_redirections_add
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Redirection à ajouter
          required: true
          schema:
            type: string
            example: "redirection@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    delete:
      summary: Supprime une redirection active sur la boite mail de l'utilisateur désigné
      tags:
        - mailbox_redirections
      operationId: mailbox_redirections_delete
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Redirection à supprimer
          required: true
          schema:
            type: string
            example: "redirection@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


# MAILBOX RECIPIENT BCC
  "/{owner}/mailbox-recipient-bcc":
    get:
      summary: Retourne la liste des adresses qui reçoivent une copie cachée de tous les courriels reçus par la boite mail de l'utilisateur désigné
      tags:
        - mailbox_recipient_bcc
      operationId: mailbox_recipient_bcc_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

  "/{owner}/mailbox-recipient-bcc/{email}":
    post:
      summary: Ajoute une adresse qui recevra une copie cachée de tous les courriels envoyés par l'utilisateur désigné
      tags:
        - mailbox_recipient_bcc
      operationId: mailbox_recipient_bcc_add
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Adresse mail à ajouter
          required: true
          schema:
            type: string
            example: "recipientbcc@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    delete:
      summary: Supprime une adresse qui recoit une copie cachée de tous les courriels envoyés par l'utilisateur désigné
      tags:
        - mailbox_recipient_bcc
      operationId: mailbox_recipient_bcc_delete
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Redirection à supprimer
          required: true
          schema:
            type: string
            example: "recipientbcc@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


# MAILBOX SENDER BCC
  "/{owner}/mailbox-sender-bcc":
    get:
      summary: Retourne la liste des adresses qui reçoivent une copie cachée de tous les courriels envoyés depuis la boite mail de l'utilisateur désigné
      tags:
        - mailbox_sender_bcc
      operationId: mailbox_sender_bcc_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

  "/{owner}/mailbox-sender-bcc/{email}":
    post:
      summary: Ajoute une adresse qui recevra une copie cachée de tous les courriels envoyés par l'utilisateur désigné
      tags:
        - mailbox_sender_bcc
      operationId: mailbox_sender_bcc_add
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Adresse mail à ajouter
          required: true
          schema:
            type: string
            example: "senderbcc@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    delete:
      summary: Supprime une adresse qui recoit une copie cachée de tous les courriels envoyés par l'utilisateur désigné
      tags:
        - mailbox_sender_bcc
      operationId: mailbox_sender_bcc_delete
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: email
          in: path
          description: Redirection à supprimer
          required: true
          schema:
            type: string
            example: "senderbcc@demoptimus.ovh"
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


  # MAILBOX QUOTA
  "/{owner}/mailbox-quota/{size}":
    patch:
      summary: Fixe l'espace disque maximal que la boîte mail peut utiliser (réservé aux administrateurs)
      tags:
        - mailbox_quota
      operationId: mailbox_quota_change
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: size
          in: path
          description: Taille de la boite mail en octets. Indiquez 0 pour une taille illimitée
          required: true
          schema:
            type: integer
            example: 1073741824
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


  # MAILBOX POSTMASTER
  "/{owner}/mailbox-postmaster":
    get:
      summary: Vérifie si l'utilisateur est un postmaster (le postmaster reçoit les messages d'erreur du serveur mail)
      tags:
        - mailbox_postmaster
      operationId: mailbox_postmater_read
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    post:
      summary: Attribue le rôle postmaster à l'utilisateur (le postmaster reçoit les messages d'erreur du serveur mail)
      tags:
        - mailbox_postmaster
      operationId: mailbox_postmater_create
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

    delete:
      summary: Supprime le rôle postmaster pour l'utilisateur (le postmaster reçoit les messages d'erreur du serveur mail)
      tags:
        - mailbox_postmaster
      operationId: mailbox_postmater_delete
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []


  # SERVICE
  "/service":
    get:
      summary: Retourne les informations sur le service (version installée, version disponible)
      tags:
        - service
      operationId: service_read
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

  "/{owner}/service":
    post:
      summary: Installe le service pour l'utilisateur désigné
      tags:
        - service
      operationId: service_install
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []
    delete:
      summary: Désinstalle le service pour l'utilisateur désigné
      tags:
        - service
      operationId: service_uninstall
      parameters:
        - name: owner
          in: path
          description: Identifiant de l'utilisateur propriétaire
          required: true
          schema:
            type: integer
            example: 1
        - name: remove_data
          in: query
          description: Précise si les données de l'utilisateur doivent être détruites
          required: true
          schema:
            type: boolean
            enum: [ false, true]
      responses:
        default:
          $ref: "#/components/responses/standardResponse"
      security:
        - cookieAuth: []

components:
  schemas:
    mailbox:
      type: object
      properties:
        id:
          type: integer
          nullable: false
          default: 0
        email:
          type: string
          nullable: false
        quota:
          type: integer
          nullable: true
        status:
          type: boolean
          nullable: false
        aliases:
          type: string
        redirections:
          type: string
        sender_bcc:
          type: string
        recipient_bcc:
          type: string

  securitySchemes:
    cookieAuth:
      type: apiKey
      in: cookie
      name: token

  responses:
    standardResponse:
      description: Réponse standard
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
              message:
                type: string
              data:
                type: string
