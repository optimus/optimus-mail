FROM php:7.4-fpm-bullseye

ARG DEBIAN_FRONTEND=noninteractive

RUN usermod --uid 219 www-data && groupmod --gid 219 www-data
RUN groupadd mailboxes --gid 203 && useradd --uid 203 mailboxes -g mailboxes -s /bin/false -d /srv/mailboxes 
RUN usermod -a -G mailboxes www-data
RUN groupadd opendkim --gid 204 && useradd --uid 204 opendkim -g opendkim -s /bin/false -d /srv/dkim 
RUN groupadd opendmarc --gid 205 && useradd --uid 205 opendmarc -g opendmarc -s /bin/false -d /srv/dkim 

#POSTFIX
RUN apt-get update && apt-get -qq -y install postfix postfix-mysql sasl2-bin libsasl2-modules libsasl2-modules-sql
COPY ./optimus-mail/postfix/saslauthd /etc/default/saslauthd
COPY --chmod=0644 ./optimus-mail/postfix/ /etc/postfix/

#DOVECOT
RUN apt-get update && apt-get -qq -y install dovecot-imapd dovecot-sieve dovecot-managesieved dovecot-mysql
COPY --chown=root:dovecot --chmod=0644 ./optimus-mail/dovecot/dovecot.conf ./optimus-mail/dovecot/dovecot-sql-passdb.conf ./optimus-mail/dovecot/dovecot-sql-passdb-autologin.conf ./optimus-mail/dovecot/dovecot-sql-userdb.conf ./optimus-mail/dovecot/dovecot-dict-sql.conf ./optimus-mail/dovecot/quota_warning.sh /etc/dovecot/
COPY --chown=www-data:mailboxes ./optimus-mail/dovecot/default.sieve /srv/default.sieve

#OPENDMARC
RUN apt-get update && apt-get -qq -y install opendmarc
RUN mkdir -p /etc/opendmarc/
RUN mkdir -p /var/spool/postfix/opendmarc
RUN chown opendmarc:opendmarc /var/spool/postfix/opendmarc -R
RUN chmod 750 /var/spool/postfix/opendmarc/ -R
COPY --chown=root:root --chmod=0644 ./optimus-mail/opendmarc/etc/opendmarc.conf /etc/opendmarc.conf
COPY --chown=root:root --chmod=0644 ./optimus-mail/opendmarc/etc/default/opendmarc /etc/default/opendmarc
COPY --chown=root:root --chmod=0644 ./optimus-mail/opendmarc/etc/opendmarc/ignore.hosts /etc/opendmarc/ignore.hosts

#OPENDKIM
RUN apt-get update && apt-get -qq -y install opendkim opendkim-tools procps
COPY --chown=root:root --chmod=0644 ./optimus-mail/opendkim/etc/opendkim.conf /etc/opendkim.conf
COPY --chown=root:root --chmod=0644 ./optimus-mail/opendkim/etc/default/opendkim /etc/default/opendkim

#AUTOCONFIG / AUTODISCOVER
COPY --chown=www-data:www-data --chmod=0644 ./optimus-mail/autoconfig.php /srv/autoconfig.php
COPY --chown=www-data:www-data --chmod=0644 ./optimus-mail/autodiscover.php /srv/autodiscover.php

# RUN apt install -y spamass-milter 
# RUN apt install -y clamav-milter mailutils 

#OPTIMUS
RUN apt-get update && apt-get -qq -y install rsyslog
RUN sed -i '/imklog/s/^/#/' /etc/rsyslog.conf
RUN docker-php-ext-install pdo_mysql pcntl
RUN echo "user = root\n" >> /usr/local/etc/php-fpm.d/zzz-docker.conf
RUN echo "group = root\n" >> /usr/local/etc/php-fpm.d/zzz-docker.conf

COPY ./optimus-libs/init.php ./optimus-mail/manifest.json /srv/
COPY ./optimus-mail/api /srv/api
COPY ./optimus-mail/client /srv/client
COPY ./optimus-mail/sql /srv/sql
COPY ./optimus-mail/vhosts /srv/vhosts

COPY ./optimus-mail/php.ini /usr/local/etc/php/conf.d

RUN mkdir -p /srv/api/libs
COPY \
	./optimus-libs/datatables.php \
	./optimus-libs/functions.php \
	./optimus-libs/JWT.php \
	./optimus-libs/ovh.php \
	./optimus-libs/websocket_client.php \
	/srv/api/libs/

EXPOSE 25
EXPOSE 143
EXPOSE 465
EXPOSE 587
EXPOSE 993

ENTRYPOINT ["php", "/srv/init.php"]